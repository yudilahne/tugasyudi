I Gede Ngurah Yudi Saputra


# 01. React Native Project Alert (Individual)

#### Design Reference

Figma : https://www.figma.com/file/GhoT1FVgqf9taaGrw6K5cu/Untitled?node-id=0%3A1

#### Task and Requirements

Buatlah sebuah aplikasi **todo list** sederhana yang menggunakan **react native** dan list berikut :

  1. **Authorization** (_AsyncStorage_) _Optional_
  2. **Redux** _Optional_
  3. **Unit Testing** (_Jest_)
  4. **CI / CD** (_Gitlab Runner_)

#### Steps?
  1. **Clone** this repo
  2. **Create a branch** with your name
  3. Make a **project inside your branch** _(don't forget to implement the requirements)_
  4. If you have done for it. **push your code** to your branch
  5. let's **make a record** (about how to use all of features in your app)
  6. Then, send your recording to **irwan syafani** via **personal message telegram**

💣 **Deadline** : Selasa, 9 Maret 2021 - 12.00 WIB

remember make the app simple 😉

Happy Coding 👋

tes
