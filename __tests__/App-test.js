/**
 * @format
 */

// import 'react-native';
import React from 'react';
// import App from '../App';
// import {name as appName} from '../app.json';
import {act, create} from 'react-test-renderer'
import DummyScreen from '../src/features/auth/screens/DummyScreen'
import {tambah, bagi} from '../src/assets/mathoperation'
// import jestConfig from '../jest.config';
// import renderer from 'react-test-renderer';


// Note: test renderer must be required after react-native.


// it('renders correctly', () => {
//   renderer.create(<App />);
//   // renderer.create(appName, () => App)
// });

const tree = create(<DummyScreen/>)

test('tambah-tambahan', () => {
  expect(tambah(1,2)).toEqual(3)
})

test('bagi-bagian', () => {
  expect(bagi(4,2)).toEqual(2)
})

test('snapshot', () => {
  expect(tree).toMatchSnapshot()
})

test('button pressed', () => {
  //press button
  const button = tree.root.findByProps({testID: 'myButton'}).props;
  act(() => button.onPress())

  //expect text to equal "Clicked"
  const text = tree.root.findByProps({testID: 'myText'}).props;
  expect(text.children).toEqual('Clicked')
})

// test('call timeout', () => {
//   act(() => jest.runAllTimers());

//   const text = tree.root.findByProps({testID: 'myText'}).props;
//   expect(text.children).toEqual('timeout is called')
// })

