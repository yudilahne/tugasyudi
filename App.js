import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Provider} from 'react-redux';
import store from './src/redux/store';

import RootNavigation from './src/routes/root';

// const store = configureStore();

const myTheme = {
  ...DefaultTheme,
  roundness: 25,
  colors: {
    ...DefaultTheme.colors,
    primary: '#ffffff',
    accent: '#FF8B8B',
  },
};

const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={myTheme}>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
      </PaperProvider>
    </Provider>
  );
};

export default App;
