import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Appbar, Avatar, useTheme} from 'react-native-paper';
import {createStackNavigator} from '@react-navigation/stack';
import {MaterialCommunityIcons} from 'react-native-vector-icons/MaterialCommunityIcons';

import LoginScreen from '../features/auth/screens/LoginScreen';
import RegisterScreen from '../features/auth/screens/RegisterScreen';
import HomeScreen from '../features/auth/screens/HomeScreen';

const Stack = createStackNavigator();

const Header = ({scene, previous, navigation}) => {
  return (
    <Appbar.Header>
      {previous && <Appbar.BackAction onPress={navigation.goBack} />}
      <Appbar.Content
        titleStyle={{fontWeight: 'bold', fontSize: 24, color: 'white'}}
        title={scene.route.name}
      />
      <Appbar.Action color="white" icon="magnify" />
    </Appbar.Header>
  );
};

const root = () => {
  return (
    <Stack.Navigator
      headerMode="screen"
      screenOptions={{
        header: (props) => <Header {...props} />,
      }}>
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Register" component={RegisterScreen} />
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
  );
};

export default root;
