import {combineReducers} from 'redux';
import AuthReducer from '../features/auth/reducers/AuthReducer';

const IndexReducer = combineReducers({auth: AuthReducer});

export default IndexReducer;
