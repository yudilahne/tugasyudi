import {createStore, applyMiddleware} from 'redux';
import rootReducer from './combine_reducer.js';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

const middlewares = [promise, thunk];

// if (__DEV__) {
//   const createDebugger = require('redux-flipper').default;
//   middlewares.push(createDebugger());
// }

const configureStore = createStore(
  rootReducer,
  applyMiddleware(...middlewares),
);

export default configureStore;
