import {ActionType} from '../../../utils/constant.js';
import client from '../../../utils/service.js';
import axios from 'axios';

export const LoginAction = (data) => {
  return async (dispatch) => {
    // POST LOGIN REQUEST
    dispatch({
      type: ActionType.LOGIN_REQUEST,
    });

    try {
      let res = await client.post('/auth/login', data);

      // CATCH POST LOGIN SUCCESS
      dispatch({
        type: ActionType.LOGIN_SUCCESS,
        payload: {
          email: data.email,
          token: res.data.access_token,
        },
      });
    } catch (err) {
      // CATCH POST LOGIN ERROR
      dispatch({
        type: ActionType.LOGIN_FAILED,
        error: err,
      });
    }
  };
};

export const LogoutAction = () => {
  console.log('LOGOUT SUCCESS');

  return {
    type: ActionType.LOGOUT,
    payload: {
      email: null,
      token: null,
    },
  };
};
