import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  judul: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 5,

    borderWidth: 2,
    borderColor: 'lightgray',
    borderRadius: 20,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  username: {
    fontSize: 20,
    fontWeight: '500',
  },
  textWrapper: {
    justifyContent: 'center',
    marginLeft: 20,
  },
});

export default styles;
