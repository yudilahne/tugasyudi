import {ActionType} from '../../../utils/constant';

// initial state authReducer
const initialState = {
  isLoading: false,
  payload: {},
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionType.LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ActionType.LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        payload: action.payload,
      };
    case ActionType.LOGIN_FAILED:
      return {
        ...state,
        payload: {},
        isLoading: false,
        error: action.error,
      };
    case ActionType.LOGOUT:
      return state;
    default:
      return state;
  }
};
