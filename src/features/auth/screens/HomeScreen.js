import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {Button} from 'react-native-paper';
import axios from 'axios';
import styles from '../styles/HomeStyles';
import Images from '../../../themes/image';
import AsyncStorage from '@react-native-async-storage/async-storage';

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
      token: null,
    };
  }

  componentDidMount() {
    this.getToken();
    this.getData();
  }

  getToken = async () => {
    try {
      let item = await AsyncStorage.getItem('token');
      if (item) {
        this.setState({token: item});
        this.getData();
        alert('Token acquired!');
      }
    } catch (err) {
      alert('Gagal mendapatkan token');
    }
  };

  
  getData = async () => {
    // async
    try {
      let res = await axios.get('https://reqres.in/api/users');
      this.setState({data: res.data.data});
      console.log(res.data.data)
    } catch (err) {
      alert(err.message);
    }
  };

  render() {
    return (
      <View>
        <SafeAreaView style={{flexDirection: 'row', alignItems: 'center'}}>
          
          <Text style={{marginLeft: 10, fontSize: 20}}>...</Text>
        </SafeAreaView>
        <ScrollView>
          {this.state.data &&
            this.state.data.map((item, i) => {
              return (
                <TouchableOpacity style={styles.container} key={i}>
                  <Image style={styles.avatar} source={{uri: item.avatar}} />

                  <View style={styles.textWrapper}>
                    <Text style={styles.username}>{item.first_name}</Text>
                    <Text>{item.email}</Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          <Button
            style={{marginTop: 10}}
            mode="contained"
            onPress={() =>
              this.props.navigation.reset({
                routes: [{name: 'Login'}],
              })
            }>
            Logout
          </Button>
        </ScrollView>
      </View>
    );
  }
}

export default HomeScreen;
