import React, {Component} from 'react';
import {View} from 'react-native';
import {TextInput, Button, HelperText} from 'react-native-paper';
import axios from 'axios';

class RegisterScreen extends Component {
  constructor() {
    super();
    this.state = {
      email: 'eve.holt@reqres.in',
      password: null,
      confirmPassword: false,
      onError: false,
      errorMsg: null,
      pwdNotMatch: false,
      showPassword: false,
    };
  }

  doRegister = () => {
    let data = {
      password: this.state.password,
      email: this.state.email,
    };
    axios
      .post('https://reqres.in/api/register', data)
      .then((res) => {
        alert('Register Success! ' + res.data.token);
      })
      .catch((err) => {
        this.setState({
          onError: true,
          errorMsg: err.message,
        });
      });
  };

  renderEyeIcon = () => {
    return (
      <TextInput.Icon
        onPress={() => this.setState({showPassword: !this.state.showPassword})}
        name={'eye'}
        color={'grey'}
      />
    );
  };

  render() {
    return (
      <View style={{marginHorizontal: 20, flex: 1, justifyContent: 'center'}}>
        <TextInput
          label="Email"
          mode="outlined"
          value={this.state.email}
          onChangeText={(text) => this.setState({email: text})}
        />
        <TextInput
          label="Password"
          mode="outlined"
          right={this.renderEyeIcon()}
          secureTextEntry={this.state.showPassword}
          value={this.state.password}
          onChangeText={(text) => this.setState({password: text})}
        />
        <TextInput
          label="Confirm Password"
          mode="outlined"
          right={this.renderEyeIcon()}
          secureTextEntry={true}
          value={this.state.confirmPassword}
          onChangeText={(text) => this.setState({confirmPassword: text})}
        />
        <HelperText type="error" visible={this.state.onError}>
          {this.state.errorMsg ? this.state.errorMsg : 'Something Went Wrong!'}
        </HelperText>
        <HelperText type="error" visible={this.state.pwdNotMatch}>
          {this.state.pwdNotMatch ? 'Password do not match!' : ''}
        </HelperText>
        <Button
          onPress={() => {
            if (this.state.confirmPassword === this.state.password) {
              this.setState({pwdNotMatch: false});
              this.doRegister();
            } else {
              this.setState({pwdNotMatch: true});
            }
          }}>
          Submit
        </Button>
      </View>
    );
  }
}

export default RegisterScreen;
