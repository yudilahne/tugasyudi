import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
} from 'react-native';
import {
  TextInput,
  Button,
  HelperText,
  Modal,
  ActivityIndicator,
} from 'react-native-paper';

import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StackActions} from '@react-navigation/native';

import {LoginAction, LogoutAction} from '../action/AuthAction.js';
import {connect} from 'react-redux';
import {ActionType} from '../../../utils/constant.js';

class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      token: null,
      email: null,
      password: null,
      onError: false,
      errorMsg: null,
      isLoading: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.authReducer !== this.props.authReducer) {
      if (this.props.authReducer.payload) {
        this.setState({token: this.props.authReducer.payload.token}, () => {
          // this.props.navigation.dispatch(StackActions.replace('Home'));
          this.props.navigation.reset({
            routes: [{name: 'Home'}],
          });
        });
      }
    }
  }

  doLogin = async () => {
    let data = {
      email: this.state.email,
      password: this.state.password,
    };
    await this.props.Login(data);
  };

  storeToken = async () => {
    try {
      await AsyncStorage.setItem('token', this.state.token);
      this.props.navigation.reset({
        routes: [{name: 'Home'}],
      });
    } catch (e) {
      alert('gagal menyimpan data');
    }
  };

  render() {
    console.log(this.props);
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView
          keyboardVerticalOffset={40}
          behavior="padding"
          style={{flex: 1}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View
              style={{
                flex: 1,
                paddingHorizontal: 20,
                justifyContent: 'space-around',
              }}>
            
              <View>
                <View>
                  <Text style={{ 
                    textAlign:'center', 
                    fontSize: 85,
                    fontWeight: 'bold',
                    
                    }}> Login </Text>
                </View>
                <TextInput
                  label="Email"
                  mode="outlined"
                  value={this.state.email}
                  onChangeText={(text) => this.setState({email: text})}
                />
                <TextInput
                  label="Password"
                  mode="outlined"
                  secureTextEntry={true}
                  value={this.state.password}
                  onChangeText={(text) => this.setState({password: text})}
                />
                <HelperText type="error" visible={this.state.onError}>
                  {this.state.errorMsg
                    ? this.state.errorMsg
                    : 'Something Went Wrong!'}
                </HelperText>
                <Button
                  style={{marginTop: 10}}
                  mode="contained"
                  onPress={() => {
                    this.doLogin();
                  }}>
                  Login
                </Button>
                <Button
                  onPress={() => this.props.navigation.navigate('Register')}>
                  Register
                </Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
        <Modal visible={this.state.isLoading} dismissable={false}>
          <ActivityIndicator size={70} />
        </Modal>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  authReducer: state.auth,
});

const mapDispatchToProps = (dispatch) => {
  return {
    Login: (data) => dispatch(LoginAction(data)),
    Logout: () => dispatch(LogoutAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
