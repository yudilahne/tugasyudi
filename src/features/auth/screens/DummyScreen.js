import React, { useEffect, useState } from 'react';
import {Button, View, Text} from 'react-native';

const DummyScreen = () => {

    useEffect(() => {
        setTimeout(() => {
            setStatus('timeout is called');
        }, 1000)
    }, [])

    const [status, setStatus] = useState(null)
    return(
        <View style={{backgroundColor:'red'}}>
            <Text testID='myText'>{status}</Text>
            <Button testID='myButton' onPress={() => setStatus('Clicked')} title="press me" ></Button>
        </View>
    )
}

export default DummyScreen;
