export class ActionType {
  static LOGIN_REQUEST = 'LOGIN REQUEST';
  static LOGIN_SUCCESS = 'LOGIN SUCCESS';
  static LOGIN_FAILED = 'LOGIN FAILED';
  static LOGOUT = 'LOGOUT';
}
