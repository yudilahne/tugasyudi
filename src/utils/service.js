import axios from 'axios';

const local_url = 'http://localhost:3000';

const client = axios.create({
  baseURL: local_url,
  timeout: 10000,
});
export default client;
